# Generated by Django 4.0.3 on 2022-10-20 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0006_remove_hat_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='picture',
            field=models.URLField(null=True),
        ),
    ]
