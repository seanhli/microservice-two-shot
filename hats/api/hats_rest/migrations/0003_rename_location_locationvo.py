# Generated by Django 4.0.3 on 2022-10-20 00:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_remove_hat_location'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Location',
            new_name='LocationVO',
        ),
    ]
