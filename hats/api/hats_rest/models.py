from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=150)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"

    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    picture = models.URLField(null=True)
    location = models.ForeignKey(LocationVO, related_name="hats",on_delete=models.PROTECT,null=True)
