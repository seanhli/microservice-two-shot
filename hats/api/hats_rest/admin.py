from django.contrib import admin
from .models import LocationVO, Hat

admin.site.register(LocationVO)
admin.site.register(Hat)

# Register your models here.
