from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat
from .acl import get_hat_picture


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id","fabric","style","color","picture"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric","style","color","picture","location"]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_id=None):
    '''
    {
        "fabric": char,
        "style": char,
        "color": char,
        "picture": url,
        "location": {
            "closet_name":,
            "section_numbers":,
            "shelf_number":,
        }
    }
    '''
    if request.method == "GET":
        if location_id == None:
            hats = Hat.objects.all()
        else:
            hats = Hat.objects.filter(location=location_id)
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            content["picture"] = get_hat_picture(
                content["fabric"],
                content["style"],
                content["color"]
            )
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
