# Wardrobify

Team:

* Daisy Song - Shoes microservice
* Sean Li - Hat microservice

## Overall design / Approach

- Check how Wardrobe monolith is set up (what do the models look like, what do the endpoints look like, how are the permissions for CORS set up)
- Review microservices for shoes and hats (and install each app to their respective django project)
- Dockerfy each microservice (and wardrobe if it isn't already)
- Create models for each microservice
- Create Restful API views to get, post, push, delete, and create data from models
- Create consumer file that polls for information from wardrobe to add/update to models (using RabbitMQ)
- Create URLs for necessary views following restful convention
- Set up React to utilize restful API to show information on front end

## Shoes microservice
Requirements:
1. Register Django app(shoes_rest) in Django project(shoes_project) INSTALLED_APPS and register in admin
2. The Shoe model manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.
3.Create RESTful APIs to get a list of shoes, create a new shoe, and delete a shoe by building encoders
4.Create React component(s) to show a list of all shoes and their details.
5.Create React component(s) to show a form to create a new shoe.
6.Provide a way to delete a shoe.
7.Route the existing navigation links to your components.
8.Build poller function to get Bin data from the Wardrobe API to shoes/poll
*Your service's poller will poll the base URL http://wardrobe-api:8000.
9.Delete functionality-onClick handler
*<button onClick={() => this.delete(hat.id)}>
Commands that may need:
   * docker volume create pgdata
   * docker-compose build
   * docker-compose up
Delete database:
   * Stop all services
   * Run docker container prune -f
   * Run docker volume rm pgdata
   * Run docker volume create pgdata
   * Run docker-compose up
Migrations:
   * Run docker exec -it «api-container-name» bash to connect to the running service that contains your API service.
   * Python manage.py makemigrations
   * Python manage.py migrate
   * Stop your poller service using Docker Desktop.
   * Start your poller service using Docker Desktop.
  Git Commands:
   * git checkout main
   * git pull
   * git checkout shoesbranch
   * git merge main
## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Goal:
- Create a microservice that can create hat objects
- Visualize microservice elements through React and enable users to interact with Hat objects

Models used:
- Hat model (required attributes: fabric, style, color, location (foreign key))
- Location model (location objects created via poller calling wardrobe API)

Integration with wardrobe:
- Hat poller calls wardrobe api endpoint for retrieving list of locations
- Hat poller creates/updates location objects in hat microservice DB
- Hat views are RESTful apis that allow for retrieval, creation, and deletion of hat objects

React:
- Route to main page a path for hat list (as route's index) and a child path for creating a new hat
