from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class BinVoEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
        ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["id", "manufacturer", "name", "color", "picture_url"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "name", "color", "picture_url", "bin"]
    encoders = {"bin": BinVoEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    '''
    {
        "manufacturer": char,
        "name": char,
        "color": char,
        "picture_url": url,
        "bin": {
            "closet_name":,
            "bin_number":,
            "bin_size":,
        }
    }
    '''
    if request.method == "GET":
            shoe = Shoes.objects.all()
            return JsonResponse({
             "shoes": shoe},
             encoder=ShoesListEncoder,
             safe=False

    )
    else:
        content = json.loads(request.body)
        # # print("Content:::: ", content)

        # print("bin_href:::", bin_href)
        # # # /api/bins/6/
        # print("*****BinVO.objects*****", BinVO.objects.all())
        # bin_href = content["bin"]

        # print("BIN:::", bin)
        # content["bin"] = bin
        # print("****[Bin has been assigned to content]:::: ", content)
        try:
            bin_href = content["bin"]
            print(BinVO.objects.all())
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,

            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
                    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                    status=400,

            )
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False
        )
