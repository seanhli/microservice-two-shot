import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

# null value in column "bin_size" of relation "shoes_rest_binvo" violates not-null constraint

# DETAIL:  Failing row contains (445, /api/bins/9/, Hate, 5, null).

# Shoes poller polling for data
# got it
# {'bins': [{'href': '/api/bins/9/', 'id': 9, 'closet_name': 'Hate', 'bin_number': 5, 'bin_size': 2}]}
# null value in column "bin_size" of relation "shoes_rest_binvo" violates not-null constraint
# DETAIL:  Failing row contains (449, /api/bins/9/, Hate, 5, null).

def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    print("got it")
    content = json.loads(response.content)
    print(content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            #if BinVo exists with #import_href = bin["href"] then #update defaults
            #else create new BinVO with import_href = bin["href"]
            import_href = bin["href"],
            defaults={"import_href": bin["href"],
                      "closet_name": bin["closet_name"],
                      "bin_size": bin["bin_size"],
                      "bin_number": bin["bin_number"],
                      },
        )
    print(BinVO.objects.all())


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
           get_bins()

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
