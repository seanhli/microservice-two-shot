import React, { useEffect, useState } from 'react';
import { Link, Navigate } from 'react-router-dom';


function ShoesListColumn(props) {
  // console.log(props.list)
  return (
    <div className="col">
      {props.list.map(shoe => {
        console.log("********:", shoe)
        return (
          <div key={shoe.bin} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{shoe.name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.name}
              </h6>
              <p className="card-text">
                {shoe.manufacturer}
              </p>
              <p className="card-text">
                {shoe.color}
              </p>
              <button onClick={event => removeShoes(event, shoe.id)} className='btn btn-dark btn-sm'>delete</button>
            </div>
          </div>
        );

      })}
    </div>
  );

  async function  removeShoes(event, id) {
    console.log("**trying to delete id:" , id)
    const deletionURL = `http://localhost:8080/api/shoes/${id}`
    const fetchConfig = {
      method: 'delete',
    };
    const response = await fetch(deletionURL, fetchConfig)
    if (response.ok) {
      const confirmation = await response.json()
      console.log(confirmation)
    } else {
      console.log("error::: ",response)
    };
  }
}
class ShoesList extends React.Component {
  constructor(props) {
    super(props);
    // console.log(props.shoes)
    this.state = {
      shoeColumns: [[], [], []],
    };
    for (let i = 0; i < props.shoes.length; i++) {
      const item = props.shoes[i];
      this.state.shoeColumns[i % 3].push(item)
    };
  }

    // console.log("*****shoeColumns",this.state.shoeColumns)

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600" />
          <h1 className="display-5 fw-bold">Shoes List</h1>
          <div className="col-lg-6 mx-auto">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mb-3">
              <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a shoe</Link>
            </div>
            <div className="container">
              <div className="row">
                {this.state.shoeColumns.map((column, index) => {
                  return (
                    <ShoesListColumn key={index} list={column} />
                  );
                })}
              </div>

            </div>
          </div>
        </div>
      </>
    );
  }
}


export default ShoesList;
