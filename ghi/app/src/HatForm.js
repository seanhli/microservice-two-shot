import React from 'react'

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            fabric: '',
            style: '',
            color: '',
            picture: '',
            location: '',
            successflag: false,
        }
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleFabricChange = this.handleFabricChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleNewForm = this.handleNewForm.bind(this)
        this.handleStyleChange = this.handleStyleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/'
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({
                    locations: data.locations,
                })
            } else {
                throw new Error('Response not ok')
            }
        } catch (e) {
            console.log("error::: ",e)
        }
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handleFabricChange(event) {
        const value = event.target.value
        this.setState({fabric: value})
    }

    handleLocationChange(event) {
        const value = event.target.value
        this.setState({location: value})
    }

    handleStyleChange(event) {
        const value = event.target.value
        this.setState({style: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.successflag
        delete data.locations
        const json = JSON.stringify(data)

        const hatURL = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(hatURL, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()
            console.log(newHat)
            const cleared = {
                fabric: '',
                style: '',
                color: '',
                picture: '',
                location: '',
                successflag: true,
            }
            this.setState(cleared)
        }
    }

    handleNewForm(event) {
        this.setState({successflag: false})
    }

    render() {
        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://www.seussville.com/app/uploads/2019/11/character-Cat-in-the-Hat.png"/>
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                {!this.state.successflag &&
                                <form onSubmit={this.handleSubmit} id="create-hat-form">
                                    <h1 className="card-title">Got a new hat? Add it to the wardrobe!</h1>
                                    <div className="form-floating mb-3">
                                        <input placeholder="Fabric type" required type="text" onChange={this.handleFabricChange} name="fabric" id="fabric" className="form-control" value={this.state.fabric}/>
                                        <label htmlFor="fabric">Fabric</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input placeholder="Hat style" required type="text" onChange={this.handleStyleChange} name="style" id="style" className="form-control" value={this.state.style}/>
                                        <label htmlFor="style">Style</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input placeholder="Color" required type="text" onChange={this.handleColorChange} name="color" id="color" className="form-control" value={this.state.color}/>
                                        <label htmlFor="color">Color</label>
                                    </div>
                                    <div className="mb-3">
                                        <select required name="location" onChange={this.handleLocationChange} id="location" className="form-select" value={this.state.location}>
                                            <option value="">Choose a location</option>
                                            {this.state.locations.map(location => {
                                                return (
                                                    <option key={location.href} value={location.href}>
                                                        Closet: {location.closet_name} | Section: {location.section_number} | Shelf: {location.shelf_number}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Create new hat</button>
                                </form>}
                                {this.state.successflag &&
                                <div>
                                    <div className="alert alert-success mb-3" id="success-message">
                                        New hat added!
                                    </div>
                                    <button className="btn btn-lg btn-primary" onClick={this.handleNewForm}>Create another hat</button>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


}

export default HatForm
