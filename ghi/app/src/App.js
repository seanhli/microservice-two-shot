import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatsDisplay from './HatList';
import HatDetail from './HatDetail';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


function App(props) {
  if (props.hats === undefined && props.shoes === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='hats'>
            <Route index element={<HatsDisplay hats={props.hats} />} />
            <Route path='new' element={<HatForm/>} />
            <Route path=":id" element={<HatDetail/>}/>
          </Route>
          <Route path='shoes'>
            <Route index element={<ShoesList shoes={props.shoes} />} />
            <Route path='new' element={<ShoesForm/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
