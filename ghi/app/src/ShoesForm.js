import React from "react";

class ShoesForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            id: "",
            manufacturer: "",
            name: "",
            color: "",
            picture_url: "",
            bin: "",
            bins: [],

        };
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handlePictureChange = this.handlePictureChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleSubmitChange = this.handleSubmitChange.bind(this)

    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        try {
            const response = await fetch(url);

            if (!response.ok) {
                throw new Error('Error')
            } else {
                const data = await response.json();
                this.setState({bins: data.bins})
                console.log(this.state.bins)
            }
        } catch (e) {
            console.log("error::: ", e)
        }
    }
    handleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value
        this.setState({manufacturer: value})
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handlePictureChange(event) {
        const value = event.target.value
        this.setState({picture_url: value})
    }

    handleBinChange(event) {
        const value = event.target.value
        this.setState({bin: value})
    }

   async handleSubmitChange(event) {
    event.preventDefault()
    const data = {...this.state}
    // data.bin = data.bin.replace("locations", "bins")
    delete data.bins
    delete data.id
    delete data.picture_url
    const json = JSON.stringify(data)
    console.log(json)

   const shoesListUrl = 'http://localhost:8080/api/shoes/'
   const fetchConfig = {
    method: "post",
    body: json,
    headers: {
     'Content-Type': 'application/json'

   },
}
   const response = await fetch(shoesListUrl, fetchConfig)
   if (response.ok) {
      const newShoesList = await response.json();
      console.log(newShoesList)
      const cleared = {
        id: "",
        manufacturer: "",
        name: "",
        color: "",
        picture_url: "",
        bin: "",

    }
      this.setState(cleared)
   }
   }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoes</h1>
                    <form onSubmit={this.handleSubmitChange} id="create-shoes-form">
                    <div className="form-floating mb-3">
                        <input placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" onChange={this.handleManufacturerChange} value={this.state.manufacturer}/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={this.handleNameChange} value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="color" required type="text" name="color" id="colr" className="form-control" onChange={this.handleColorChange} value={this.state.color}/>
                        <label htmlFor="color">Color</label>
                    </div>
                    {/* <div className="form-floating mb-3">
                        <input placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" onChange={this.handlePictureChange} value={this.state.picture_url}/>
                        <label htmlFor="picture_url">Picture</label>
                    </div> */}
                    <div className="mb-3">
                        <select required name="bin" id="bin" className="form-select" onChange={this.handleBinChange} value={this.state.bin}>
                            <option value="">Choose a bin</option>
                            {this.state.bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                )
                            })
                            }
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>


    )
}
}
export default ShoesForm
