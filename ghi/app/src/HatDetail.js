import React, { useEffect, useState } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'

async function retrieveHat(HatID) {
    const hatResponse = await fetch(`http://localhost:8090/api/hats/${HatID}/`)
    if (hatResponse.ok) {
        const hatData = await hatResponse.json()
        return hatData
    } else {
        console.log(`Error::: ${hatResponse}`)
    }
}

function HatDetail(props) {
    const { id } = useParams()
    const [hat, setHat] = useState()
    const navigate = useNavigate()


    useEffect(() => {
        const getHat = async () => {
            if (hat === undefined) {
                const hat = await retrieveHat(id)
                setHat(hat)
            }
        }
        getHat()
    })

    const imageResize = {
        height: "auto",
        width: "300px",
    }

    if (hat === undefined) {
        return null
    }

    const deleteHat = async () => {
        const deletionURL = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: 'delete',
        }
        const response = await fetch(deletionURL, fetchConfig)
        if (response.ok) {
            const confirmation = await response.json()
            console.log(confirmation)
            navigate('/hats')
        } else {
            console.log("error::: ",response)
        }
    }

    return (
        <div className='row mt-5 ms-4'>
            <div className='col-3'>
                <img src={hat.picture} style={imageResize} className="d-block w-100" />
            </div>
            <div className='col-5 card'>
                <div className='card-body'>
                    <h5 className="card-title">{hat.fabric.toLowerCase()}, {hat.style.toLowerCase()}, {hat.color.toLowerCase()} hat</h5>
                    <h6 className="card-subtitle mb-2 text-muted">Located at {hat.location.closet_name}</h6>
                    <p className="card-text">Found in section: {hat.location.section_number}, shelf: {hat.location.shelf_number}</p>
                    <button onClick={deleteHat} className='btn btn-dark btn-lg'>Delete Hat</button>
                </div>
            </div>
        </div>
    )
}

export default HatDetail
