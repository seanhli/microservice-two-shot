import React from 'react'
import { Link } from 'react-router-dom';
import { HashRouter } from 'react-router-dom'

function HatsDisplay(props) {
    let gallery = {}
    for (let hat of props.hats) {
        if(gallery[hat.location]) {
            gallery[hat.location].push(hat)
        } else {
            gallery[hat.location] = [hat]
        }
    }
    const entries = Object.entries(gallery)

    const carouselDimension = {
        maxHeight: "200px",
        width: "auto"
    }

    const imageResize = {
        height: "200px",
        width: "auto",
    }

    const captionBoxLeft = {
        position: "absolute",
        "padding": "5px 0px 0px 25px",
        "fontWeight": "700",
        "width": "250px",
    }

    const captionBox = {
        position: "absolute",
        "padding": "5px 0px 0px 10px",
        "fontWeight": "700",
        "width": "250px",
    }

    const captionText = {
        padding:"1px 5px 1px 5px",
        "backgroundColor":"rgba(255, 255, 255, 0.7)",
    }

    return (
      <div>
        <Link className="btn btn-lg btn-dark mt-4 mb-4 ms-5" to="/hats/new">Create a new hat</Link>
        {entries.map(location => {
            const carouselID = location[0]
            return (
                <div className="row">
                    <div className="col-1">
                        <p className="mt-5 pt-5" style={{fontWeight: "900", transform: "rotate(-90deg)"}} key={location[0]}>{location[0]}</p>
                    </div>
                    <div className="col-10">
                        <div id={`${carouselID}`} className="carousel slide carousel-fade carousel-dark" data-bs-ride="carousel" style={carouselDimension}>
                            <div className="carousel-inner rounded-5 shadow-4-strong" style={carouselDimension}>
                                {location[1].map(hat => {
                                    const position = location[1].indexOf(hat)
                                    const clen = location[1].length
                                    const next = location[1][(position+1)%clen]
                                    const fol = location[1][(position+2)%clen]
                                    if (position === 0) {
                                        return (
                                            <div className="carousel-item active" key={hat.id} data-bs-interval="4500">
                                                <div className='row'>
                                                    <div className="col p-0">
                                                        <div style={captionBoxLeft}>
                                                            <span style={captionText}>{hat.fabric} | {hat.style} | {hat.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${hat.id}`}>
                                                            <img src={hat.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                    <div className="col p-0">
                                                        <div style={captionBox}>
                                                            <span style={captionText}>{next.fabric} | {next.style} | {next.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${next.id}`}>
                                                            <img src={next.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                    <div className="col p-0">
                                                        <div style={captionBox}>
                                                            <span style={captionText}>{fol.fabric} | {fol.style} | {fol.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${fol.id}`}>
                                                            <img src={fol.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    else {
                                        return (
                                            <div className="carousel-item" key={hat.id} data-bs-interval="4500">
                                                <div className='row'>
                                                    <div className="col p-0">
                                                        <div style={captionBoxLeft}>
                                                            <span style={captionText}>{hat.fabric} | {hat.style} | {hat.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${hat.id}`}>
                                                            <img src={hat.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                    <div className="col p-0">
                                                        <div style={captionBox}>
                                                            <span style={captionText}>{next.fabric} | {next.style} | {next.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${next.id}`}>
                                                            <img src={next.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                    <div className="col p-0">
                                                        <div style={captionBox}>
                                                            <span style={captionText}>{fol.fabric} | {fol.style} | {fol.color}</span>
                                                        </div>
                                                        <Link to={`/hats/${fol.id}`}>
                                                            <img src={fol.picture} style={imageResize} className="d-block w-100" />
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                })}
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target={`#${carouselID}`} data-bs-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Previous</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target={`#${carouselID}`} data-bs-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            )
        })}
      </div>
    );
}

export default HatsDisplay
